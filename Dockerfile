FROM rastasheep/alpine-node-chromium:12-alpine

WORKDIR /app

COPY . .

RUN npm i

ENTRYPOINT ["/app/entrypoint.sh"]
