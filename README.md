# odoo-trello-toggle
## How
You can use this cli application by installing `node` in your machine and running `npm install` in the root directory of this package.
After this process is completed run `npm link`. This will install the application globaly. From now on you can the cli using the command `r3-odtt`.

## Commands
* `r3-odtt odoo`: will sync all the hours you have on toggle to odoo given the time frame

* `r3-odtt missing <boardId>`: given the board id the script will notify on slack all the cards that does't have an odoo link. An optional `-p` or `--print` can be provided, this will avoid the notification on slack of those cards printing the result of the search on the screen

* `r3-odtt config`: will prompt an interactive shell with the aim of updating the configuration. On the other hand, if a `-p` or `--print` argument is provided the script will print on screen the current configuration

* `r3-odtt forgt`: given all the hours you have on toggle it will check Odoo for missing entry

## With Docker *Currently broken* 
If you are using the docker container you can config the environment through the same `.env`. Mounting a volume
or by passing it into the run command ex: `docker run odoo-trello-toggle --env-file .env`.

Volumes:

- `/app/.env`
- `/app/tasks.json` 
