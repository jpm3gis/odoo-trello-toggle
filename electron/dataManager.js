const axios = require("axios").default;
const { resolve } = require("path");
const { readFileSync, writeFileSync, existsSync } = require("fs");

const BASE_PATH_DB = resolve(__dirname, "trellostate.json");

const generateKeyFromDate = date =>
  `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;

const isDoneList = ({ name }) => name === "Done";
const isNotDoneList = ({ name }) => name !== "Done";

const getPointFromName = name => {
  const res = /\((\d|\d.\d|\d\d)\)/.exec(name);
  if (res) {
    return Number(res[1]);
  } else {
    return 0;
  }
};

const reducePrices = (acc, card) => {
  return acc + getPointFromName(card.name);
};

module.exports = function({
  task_trello_api_key,
  task_trello_oauth_token,
  boardId
}) {
  return {
    async trelloBoard() {
      const resp = await axios.get(
        `https://api.trello.com/1/boards/${boardId}/lists`,
        {
          params: {
            cards: "open",
            card_fields: "name",
            key: task_trello_api_key,
            token: task_trello_oauth_token
          }
        }
      );
      return resp.data;
    },

    saveSnapshot(trelloSnap) {
      const now = new Date();
      if (existsSync(BASE_PATH_DB)) {
        const data = readFileSync(BASE_PATH_DB).toString("utf8");
        const jsonData = JSON.parse(data);
        jsonData[generateKeyFromDate(now)] = trelloSnap;
        writeFileSync(BASE_PATH_DB, JSON.stringify(jsonData));
      } else {
        const jsonData = {};
        jsonData[generateKeyFromDate(now)] = trelloSnap;
        writeFileSync(BASE_PATH_DB, JSON.stringify(jsonData));
      }
    },

    readHistory(dateToGet) {
      try {
        const data = readFileSync(BASE_PATH_DB).toString("utf8");
        const jsonData = JSON.parse(data);
        if (dateToGet) {
          return jsonData[generateKeyFromDate(dateToGet)];
        } else {
          return jsonData;
        }
      } catch (error) {
        console.error(error);
        return dateToGet ? null : {};
      }
    },

    calculatePrices(trelloDaySnap = []) {
      return trelloDaySnap.map(list => ({
        name: list.name,
        points: list.cards ? list.cards.reduce(reducePrices, 0) : 0
      }));
    },

    formatDataForGraph(trelloSnap = {}) {
      return Object.keys(trelloSnap)
        .map(date => {
          const calculatedPriceLists = this.calculatePrices(trelloSnap[date]);
          const completed = calculatedPriceLists.find(isDoneList).points;
          const toGo = calculatedPriceLists
            .filter(isNotDoneList)
            .reduce((acc, { points }) => acc + points, 0);
          return {
            time: new Date(date),
            completed,
            toGo
          };
        })
    },

    async trelloSnapshot() {
      const trelloData = await this.trelloBoard();
      this.saveSnapshot(trelloData);
      return true;
    }
  };
};
