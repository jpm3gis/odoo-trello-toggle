const { join } = require('path')
const { readFile, writeFileSync } = require('fs')

const DB_FILE_NAME = 'state.json'

let db

async function makeDb(initialState = {}) {
    return new Promise((resolve) => {
        readFile(join(__dirname, DB_FILE_NAME), (err, data) => {
            let dbData = initialState
            if (!err) {
                try {
                    dbData = JSON.parse(data)
                } catch (error) {
                    console.log('DB', error)
                }
            }
            resolve({
                get(key) {
                    return key ? dbData[key] : dbData
                },
                set(key, val) {
                    if (!key) {
                        throw new Error('[DB] Unable to save with a null key')
                    }
                    dbData[key] = val
                },
                commit() {
                    writeFileSync(join(__dirname, DB_FILE_NAME), JSON.stringify(dbData))
                }
            })
        })
    })
}

module.exports = async function(initialState) {
    if (!db) {
        db = await makeDb(initialState)
    }
    return db
}