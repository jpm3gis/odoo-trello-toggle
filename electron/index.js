const { app, BrowserWindow, ipcMain } = require("electron");
const {
  START_UPDATING_REPO_STATUS,
  GO_IDLE,
  UPDATE_STATUS,
  WINDOW_READY,
  CLOSE_APP,
  GRAPH_DATA_LOADED,
  LOAD_GRAPH_DATA
} = require("./events");
const makeDb = require("./db");
const path = require("path");
const makeDataManager = require("./dataManager");
const cron = require("node-cron");

const initState = {
  idle: true
};

let db;
let onlyOneUpdater;

function createWindow() {
  // Crea la finestra del browser
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: false,
      preload: path.join(__dirname ? __dirname : "", "preload.js")
    }
  });

  win.on("close", function(event) {
    if (db) {
      db.commit();
    }
    win.hide();
    event.preventDefault();
  });

  app.on("activate", () => {
    win.show();
  });

  // and load the index.html of the app.
  win.loadFile("index.html");

  // Apre il Pannello degli Strumenti di Sviluppo.
  win.webContents.openDevTools();
}

app.allowRendererProcessReuse = true;

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Alcune API possono essere utilizzate solo dopo che si verifica questo evento.
app.whenReady().then(createWindow);

ipcMain.on(LOAD_GRAPH_DATA, (event, args) => {
  const dataManager = makeDataManager(args);
  const data = dataManager.formatDataForGraph(dataManager.readHistory());
  event.reply(GRAPH_DATA_LOADED, data);
});

makeDb(initState)
  .then(newDb => {
    db = newDb;
    let cronShedulerUpdatingRepoData;

    if (db.get("idle") && db.get("idle") === false) {
      db.set("idle", true);
    }

    ipcMain.on(START_UPDATING_REPO_STATUS, (event, args) => {
      const dataManager = makeDataManager(args);
      console.log("START CRON");
      cronShedulerUpdatingRepoData = cron.schedule("01 01 07 * * *", () => {
        dataManager
          .trelloSnapshot()
          .then(() => console.log("Runned"))
          .catch(console.error);
      });
      db.set("idle", false);
    });

    ipcMain.on(GO_IDLE, () => {
      if (cronShedulerUpdatingRepoData) {
        cronShedulerUpdatingRepoData.stop().destroy();
        db.set("idle", true);
      }
    });

    ipcMain.on(CLOSE_APP, () => {
      if (db) {
        db.commit();
      }
      app.quit();
      process.exit(0);
    });

    ipcMain.on(WINDOW_READY, event => {
      if (!onlyOneUpdater) {
        onlyOneUpdater = setInterval(function() {
          if (
            typeof event !== "undefined" &&
            typeof db !== "undefined" &&
            event &&
            db
          ) {
            event.reply(UPDATE_STATUS, db.get());
          }
        }, 1000);
      }
    });
  })
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
