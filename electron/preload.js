document.electron = require('electron')
document.___EVENTS = require('./events')

function storeManager() {
    const key = 'CONFIG_APP'
    return {
        get() {
            const data = window.localStorage.getItem(key)
            if (data) {
                try {
                    return JSON.parse(data)
                } catch (error) {
                    console.error('STORE_MANAGER', error)
                    return {}
                }
            } else {
                return {}
            }
        },
        save(data) {
            const strData = data ? JSON.stringify(data) : JSON.stringify({})
            window.localStorage.setItem(key, strData)
        }
    }
}

window.storeManager = storeManager