const { ipcRenderer } = document.electron
const { START_UPDATING_REPO_STATUS, GO_IDLE, UPDATE_STATUS, WINDOW_READY, CLOSE_APP } = document.___EVENTS

document.addEventListener("DOMContentLoaded", function(){
    const store = storeManager()
    const config = store.get()

    const input_trello_oauth_token = document.getElementById('trello_oauth_token')
    const input_api_key_trello = document.getElementById('api_key_trello')
    const input_board_id = document.getElementById('board_id')
    const btn_save_config = document.getElementById('save_config_btn')
    const btn_stop_monitoring = document.getElementById('stop_monitoring')
    const btn_start_monitoring = document.getElementById('start_monitoring')
    const monitoring_spinner = document.getElementById('monitoring_spinner')
    const btn_close_app = document.getElementById('close_app')

    input_trello_oauth_token.value = config.task_trello_oauth_token || ''
    input_api_key_trello.value = config.task_trello_api_key || ''
    input_board_id.value = config.boardId || ''

    ipcRenderer.on(UPDATE_STATUS, function (event, status) {
        console.log('NEW STATUS UPDATE', status)
        // Loading spinner
        monitoring_spinner.hidden = status.idle
    })

    btn_save_config.addEventListener('click', function() {
        store.save({
            task_trello_oauth_token: input_trello_oauth_token.value,
            task_trello_api_key: input_api_key_trello.value,
            boardId: input_board_id.value
        })
    })

    btn_start_monitoring.addEventListener('click', function() {
        const conf = store.get()
        ipcRenderer.send(START_UPDATING_REPO_STATUS, conf)
    })

    btn_stop_monitoring.addEventListener('click', function() {
        ipcRenderer.send(GO_IDLE)
    })

    btn_close_app.addEventListener('click', function() {
        ipcRenderer.send(CLOSE_APP)
    })

    ipcRenderer.send(WINDOW_READY)
});