#!/usr/bin/env node

const program = require("commander");
const inquirer = require("inquirer");
const config = require("./src/configurator");
const odooTaskCreation = require("./src/insert");
const odooTaskForgot = require("./src/forgot");
const makeMissingTrello = require("./src/trelloMissing");
const { diffAndReduce } = require("./src/lib");
const makeNotifier = require("./src/notifier");
const { tasksFactory, TASKS_DATASOURCES } = require("./src/tasksFactory");
const { makeManager, makeTrainer } = require("./src/nlp");
const ora = require("ora");

const validateDate = (date) => {
  if (!!Date.parse(date)) {
    return true;
  } else {
    return "Date is invalid";
  }
};

const CONFWITHPROMPT = {
  odoo_mail: { type: "input", name: "odoo_mail", message: "Your Odoo email" },
  odoo_psw: {
    type: "password",
    name: "odoo_psw",
    message: "Your Odoo password",
  },
  tasks_datasource: {
    type: "list",
    name: "tasks_datasource",
    message: "Where to get the source for the tasks",
    default: "Toggle",
    choices: ["Toggle", "File"],
  },
  tasks_start_date: {
    type: "input",
    name: "tasks_start_date",
    message: "The start date to retrieve the tasks",
    validate: validateDate,
    default: new Date(new Date().setHours(0, 0, 0, 1)).toString(),
  },
  tasks_end_date: {
    type: "input",
    name: "tasks_end_date",
    message: "The end date to retrieve the tasks",
    validate: validateDate,
    default: new Date(
      new Date(new Date().setHours(0, 0, 0, 1)).setTime(
        new Date().getTime() + 86400000
      )
    ).toString(),
  },
  task_toggle_api_key: {
    type: "input",
    name: "task_toggle_api_key",
    message: "Get a Toggle Api key",
  },
  task_trello_api_key: {
    type: "input",
    name: "task_trello_api_key",
    message: "Get a Trello Api key",
  },
  task_trello_oauth_token: {
    type: "input",
    name: "task_trello_oauth_token",
    message: "Get a Trello OAuth token",
  },
  slack_webhook_link: {
    type: "input",
    name: "slack_webhook_link",
    message: "The slack webhook",
    default:
      "https://hooks.slack.com/services/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",
  },
  number_of_days: {
    type: "number",
    name: "number_of_days",
    message: "How many times you have done reunions?",
    default: 1,
  },
  time_for_reunion: {
    type: "number",
    name: "time_for_reunion",
    message: "How much time you spent in avarage (minutes)?",
    default: 10,
  },
};

async function configShenanigans(updater) {
  if (await config.hasConf()) {
    const configuration = await config.get();
    const newConf = await inquirer.prompt(
      diffAndReduce(configuration, updater)
    );
    await config.update(newConf);
  } else {
    const newConf = await inquirer.prompt(diffAndReduce({}, updater));
    await config.update(newConf);
  }
  return await config.get();
}

require("./src/chart")(program, configShenanigans, CONFWITHPROMPT);

// Start the process to automaticly insert the task from toggle into odoo
program.command("odoo").action(async function () {
  const configuration = await configShenanigans({
    odoo_mail: CONFWITHPROMPT.odoo_mail,
    odoo_psw: CONFWITHPROMPT.odoo_psw,
    tasks_datasource: CONFWITHPROMPT.tasks_datasource,
    task_toggle_api_key: CONFWITHPROMPT.task_toggle_api_key,
    task_trello_api_key: CONFWITHPROMPT.task_trello_api_key,
    task_trello_oauth_token: CONFWITHPROMPT.task_trello_oauth_token,
    slack_webhook_link: CONFWITHPROMPT.slack_webhook_link,
  });
  const startAndEnd = await inquirer.prompt([
    CONFWITHPROMPT.tasks_start_date,
    CONFWITHPROMPT.tasks_end_date,
  ]);
  const { hasDoneReiunions } = await inquirer.prompt([
    {
      type: "list",
      name: "hasDoneReiunions",
      message: "You have done some reiunions during those days?",
      default: "Yes",
      choices: ["Yes", "No"],
    },
  ]);
  let reiunionsConfig = {};
  if (hasDoneReiunions === "Yes") {
    reiunionsConfig = await inquirer.prompt([
      CONFWITHPROMPT.number_of_days,
      CONFWITHPROMPT.time_for_reunion,
    ]);
  }
  process.env.SLACK_WEBHOOK_LINK = configuration.slack_webhook_link;
  await odooTaskCreation({
    ...configuration,
    ...startAndEnd,
    ...reiunionsConfig,
  });
});

// Manage the configuration of the cli app
program
  .command("config")
  .option("-p, --print", "Print the configuration in Json")
  .action(async function (cmd) {
    if (cmd.print) {
      const configuration = await config.get();
      console.log({
        ...configuration,
        odoo_psw: "************",
      });
    } else {
      const newConf = await inquirer.prompt(
        diffAndReduce(
          {},
          {
            odoo_mail: CONFWITHPROMPT.odoo_mail,
            odoo_psw: CONFWITHPROMPT.odoo_psw,
            tasks_datasource: CONFWITHPROMPT.tasks_datasource,
            task_toggle_api_key: CONFWITHPROMPT.task_toggle_api_key,
            task_trello_api_key: CONFWITHPROMPT.task_trello_api_key,
            task_trello_oauth_token: CONFWITHPROMPT.task_trello_oauth_token,
            slack_webhook_link: CONFWITHPROMPT.slack_webhook_link,
          }
        )
      );
      await config.update(newConf);
      console.log("Configuration has been updated");
    }
  });

// Find all the cards that are missing an odoo link given a board id
program
  .command("missing <boardId>")
  .option("-p, --print", "Only print the result")
  .action(async function (boardId, cmd) {
    const spinner = ora(
      "Looking for cards that are missing the hour link...."
    ).start();
    const configuration = await configShenanigans({
      task_trello_api_key: CONFWITHPROMPT.task_trello_api_key,
      task_trello_oauth_token: CONFWITHPROMPT.task_trello_oauth_token,
      slack_webhook_link: CONFWITHPROMPT.slack_webhook_link,
    });
    process.env.SLACK_WEBHOOK_LINK = configuration.slack_webhook_link;
    await makeMissingTrello(configuration, makeNotifier())(
      boardId,
      cmd.print
    ).catch(console.log);
    spinner.stop();
    console.log("Operation completed");
  });

// Find all the time-entry that has been forgotten to be entered from toggle to odoo
program.command("forgot").action(async function (cmd) {
  const configuration = await configShenanigans({
    odoo_mail: CONFWITHPROMPT.odoo_mail,
    odoo_psw: CONFWITHPROMPT.odoo_psw,
    tasks_datasource: CONFWITHPROMPT.tasks_datasource,
    task_toggle_api_key: CONFWITHPROMPT.task_toggle_api_key,
    task_trello_api_key: CONFWITHPROMPT.task_trello_api_key,
    task_trello_oauth_token: CONFWITHPROMPT.task_trello_oauth_token,
    slack_webhook_link: CONFWITHPROMPT.slack_webhook_link,
  });
  const startAndEnd = await inquirer.prompt([
    CONFWITHPROMPT.tasks_start_date,
    CONFWITHPROMPT.tasks_end_date,
  ]);
  process.env.SLACK_WEBHOOK_LINK = configuration.slack_webhook_link;
  await odooTaskForgot({ ...configuration, ...startAndEnd });
});

// Start the training process for the ai that should in the future replace Adriana's work
program
  .command("train")
  .option("-s, --skip", "Use a naive approach to train the ai")
  .action(async function (cmd) {
    const configuration = await configShenanigans({
      task_trello_api_key: CONFWITHPROMPT.task_trello_api_key,
      task_trello_oauth_token: CONFWITHPROMPT.task_trello_oauth_token,
      slack_webhook_link: CONFWITHPROMPT.slack_webhook_link,
    });
    const { boardId } = await inquirer.prompt([
      {
        type: "input",
        name: "boardId",
        message: "Witch board you want to use?",
      },
    ]);
    const tasks = await tasksFactory({
      ...configuration,
      boardId,
      tasks_datasource: TASKS_DATASOURCES.BOARD,
    }).extract();
    const aiManager = await makeManager();
    const aiTrainer = await makeTrainer(aiManager);
    for (let index = 0; index < tasks.length; index++) {
      const task = tasks[index];
      if (cmd.skip) {
        aiTrainer.teach(task, task.link);
        aiTrainer.addMeaning(task.link, task.link);
      } else {
        const { meaning } = await inquirer.prompt([
          {
            type: "input",
            name: "meaning",
            message: `Witch meaning you attribuite to this card? \n${task.info.name}`,
          },
        ]);
        if (meaning === "done") {
          await aiTrainer.done();
          return true;
        }
        aiTrainer.teach(task, meaning);
        aiTrainer.addMeaning(meaning, task.link);
      }
    }
    await aiTrainer.done();
  });

// Test the prediction of the ai against a given board
program.command("test").action(async function (cmd) {
  const configuration = await configShenanigans({
    task_trello_api_key: CONFWITHPROMPT.task_trello_api_key,
    task_trello_oauth_token: CONFWITHPROMPT.task_trello_oauth_token,
    slack_webhook_link: CONFWITHPROMPT.slack_webhook_link,
  });
  const { boardId } = await inquirer.prompt([
    {
      type: "input",
      name: "boardId",
      message: "Witch board you want to use?",
    },
  ]);
  const tasks = await tasksFactory({
    ...configuration,
    boardId,
    tasks_datasource: TASKS_DATASOURCES.BOARD,
  }).extract();
  const aiManager = await makeManager();
  let right = 0,
    wrong = 0;
  for (let index = 0; index < tasks.length; index++) {
    const task = tasks[index];
    const link = await aiManager.guess(task);
    if (link.answer === task.link) {
      right++;
      console.log(`RIGHT GUESS: ${task.info.name}`);
    } else {
      wrong++;
      console.log(`WRONG GUESS: ${task.info.name}`);
    }
  }
  console.log(`RIGHT: ${right} WRONG: ${wrong}`);
});

program.command("getlink <name>").action(async function (name, cmd) {
  const aiManager = await makeManager();
  const task = {
    info: {
      name,
    },
  };
  const { answer } = await aiManager.guess(task);
  console.log(`Link prediction ${name}:\n`, answer);
});

program.parse(process.argv);
