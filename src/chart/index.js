const makeDataManager = require("../../electron/dataManager");
const cron = require("node-cron");
const puppeteer = require("puppeteer");
const { resolve, join } = require("path");

const GRAPH_HTML_PATH = resolve(__dirname, "..", "..", "assets", "graph.html");

module.exports = function(program, configShenanigans, CONFWITHPROMPT) {
  program.command("updatechart <boardId>").action(async function(boardId) {
    const configuration = await configShenanigans({
      task_trello_api_key: CONFWITHPROMPT.task_trello_api_key,
      task_trello_oauth_token: CONFWITHPROMPT.task_trello_oauth_token
    });
    const dataManager = makeDataManager({
      ...configuration,
      boardId
    });
    console.log(`Reading from board id: ${boardId}`);
    await dataManager.trelloSnapshot();
    const snap = dataManager.readHistory(new Date());
    console.log(dataManager.calculatePrices(snap));
    console.log("Done!");
  });

  program.command("monitorit <boardId>").action(async function(boardId) {
    const configuration = await configShenanigans({
        task_trello_api_key: CONFWITHPROMPT.task_trello_api_key,
        task_trello_oauth_token: CONFWITHPROMPT.task_trello_oauth_token
    });
    const dataManager = makeDataManager({
      ...configuration,
      boardId
    });
    console.log('START CRON')
    cron.schedule("01 01 07 * * *", () => {
      dataManager
        .trelloSnapshot()
        .then(() => console.log("Runned"))
        .catch(console.error);
    });
  });

  program.command("chart").action(async function(boardId) {
    const configuration = await configShenanigans({
        task_trello_api_key: CONFWITHPROMPT.task_trello_api_key,
        task_trello_oauth_token: CONFWITHPROMPT.task_trello_oauth_token
      });
    const dataManager = makeDataManager({
        ...configuration,
        boardId
      });
    const pathHtml = `file://${GRAPH_HTML_PATH}`;
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto(pathHtml);
    const allData = dataManager.formatDataForGraph(dataManager.readHistory());
    await page.evaluate((allData) => {
        window.__loadGraph__(allData)
    }, allData)
  });
};
