const { resolve } = require("path");
const { readFile, writeFile, unlink } = require("fs");

const configFile = resolve(__dirname, "..", "configuration.json");

const readJsonFile = path =>
  new Promise((res, rej) => {
    readFile(path, (err, data) => {
      if (err) {
        if (err.code === "ENOENT") {
          return res({});
        } else {
          return rej(err);
        }
      }
      const content = data.toString();
      if (content.length > 0) {
        return res(JSON.parse(data.toString()));
      } else {
        return res({});
      }
    });
  });

const writeJsonFile = (path, content) =>
  new Promise((res, rej) => {
    const stringContent = JSON.stringify(content);
    writeFile(path, stringContent, err => {
      if (err) {
        return rej(err);
      }
      return res();
    });
  });

module.exports = {
  async hasConf() {
    const config = await readJsonFile(configFile)
    return Object.keys(config).length > 0
  },
  async get() {
    return await readJsonFile(configFile);
  },
  async update(config) {
    const prevConf = await readJsonFile(configFile);
    const newConf = {
      ...prevConf,
      ...config
    };
    await writeJsonFile(configFile, newConf);
  },
  delete() {
    return new Promise((res, rej) => {
      unlink(configFile, err => {
        if (err) {
          return rej(err);
        }
        return res();
      });
    });
  }
};
