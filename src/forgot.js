const puppeteer = require("puppeteer");
const { login, isTaskAlreadyBeenInserted, printTask } = require("./odoo");
const { tasksFactory } = require("./tasksFactory");
const { makePuppeteerConfig } = require('./lib')

module.exports = async config => {
  let puppeteerConfig = makePuppeteerConfig()
  const tasks = await tasksFactory(config).extract();
  const browser = await puppeteer.launch(puppeteerConfig);
  const page = await browser.newPage();
  await login(page, config.odoo_mail, config.odoo_psw)
  console.log('Looking for missing tasks:\n')
  const fouded = tasks.length
  let missing = 0
  for (let index = 0; index < tasks.length; index++) {
    const task = tasks[index];
    const hasBeenInserted = await isTaskAlreadyBeenInserted(page, task.link, task.info)
    if (!hasBeenInserted) {
      missing++
      console.log(printTask(task), '\n')
    }
  }
  console.log('Tot: ', fouded)
  console.log('Missing: ', missing)
  await browser.close();
};