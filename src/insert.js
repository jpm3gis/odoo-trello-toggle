const puppeteer = require("puppeteer");
const { login, insertIntask, printTask } = require("./odoo");
const { tasksFactory } = require("./tasksFactory");
const { makePuppeteerConfig } = require('./lib')

async function insertInOdoo(page, tasks = [], config) {
  let inserted = 0;
  let errored = [];
  await login(page, config.odoo_mail, config.odoo_psw);
  for (let index = 0; index < tasks.length; index++) {
    const task = tasks[index];
    console.log("ODOO CREATION: ", printTask(task));
    try {
      await insertIntask(page, task.link, task.info);
      inserted++;
    } catch (error) {
      console.log("ODOO ERROR INSERT TASK: ", printTask(task));
      errored.push(task);
    }
  }
  return [inserted, errored];
}

/*
  Task
  {
    link: String -> link della pagina della task in odoo,
    info: {
      date: Data in qualunque formato compatibile con js,
      tot: Il totale delle ore in formato HH:MM,
      name: Il nome della task svolta
    } 
  }
*/

module.exports = async config => {
  let puppeteerConfig = makePuppeteerConfig()
  let reunionTasks = [];
  if (config.number_of_days && config.time_for_reunion) {
    reunionTasks = await tasksFactory({
      ...config,
      tasks_datasource: "Reunion"
    }).extract();
  }
  const tasks = await tasksFactory(config).extract();
  const browser = await puppeteer.launch(puppeteerConfig);
  const page = await browser.newPage();
  const allTasks = [...tasks, ...reunionTasks];
  const [inserted, errored] = await insertInOdoo(page, allTasks, config);
  await browser.close();
  console.log(
    "OPERATION COMPLETED \n",
    `Tot: ${allTasks.length} \n`,
    `Inserted succ: ${inserted}`
  );
  if (errored.length > 0) {
    console.log("############## ERRORS ################");
    console.log(
      errored.map(printTask).reduce((acc, a) => `${a}\n${acc}`),
      ""
    );
    console.log("############## ERRORS ################");
  }
};
