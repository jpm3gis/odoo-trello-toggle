function delay(time) {
  return () => new Promise((res, _) => {
    setTimeout(res, time);
  })
}

function appendzero(str) {
  return `${str > 9 ? str : `0${str}`}`
}

function diffAndReduce(from, using) {
  const differObject = {}
  Object.keys(using).forEach(keyUsing => {
    if (!from[keyUsing]) {
      differObject[keyUsing] = using[keyUsing]
    }
  });
  return Object.keys(differObject).map(key => differObject[key])
}

function makePuppeteerConfig() {
  const puppeteerConfig = {
    headless: false
  };
  if (process.env.CHROME_BIN) {
    console.log(`Using Chrome from: `, process.env.CHROME_BIN);
    puppeteerConfig.executablePath = process.env.CHROME_BIN;
    puppeteerConfig.args = ["--no-sandbox"];
    puppeteerConfig.headless = true;
  }
  return puppeteerConfig
}

module.exports = {
    delay,
    appendzero,
    diffAndReduce,
    makePuppeteerConfig
}