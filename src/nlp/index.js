const { NlpManager } = require('node-nlp')
const { readFileSync, writeFileSync } = require('fs')
const { resolve } = require('path')

const MODEL_FILE = resolve(__dirname, '..', '..', 'model.nlp')

async function makeManager() {
    const m = new NlpManager({ languages: ['it'] })
    try {
        const data = readFileSync(MODEL_FILE, 'utf8')
        await m.import(data)
    } catch(err) {
        console.log('NLP: Not loading the model because\n')
        console.log(err)
        console.log('\n')
    }
    return {
        native: m,
        guess: task => m.process('it', task.info.name)
    }
}

async function makeTrainer(manager) {
    return {
        teach(task, meaning) {
            manager.native.addDocument('it', task.info.name, meaning)
        },
        addMeaning(meaning, response) {
            manager.native.addAnswer('it', meaning, response);
        },
        async done() {
            await manager.native.train()
            const data = await manager.native.export(true)
            return writeFileSync(MODEL_FILE, data)
        }
    }
}

module.exports = {
    makeManager,
    makeTrainer
}