const axios = require("axios").default;

module.exports = () => {
  const slackLink = process.env.SLACK_WEBHOOK_LINK;

  if (!slackLink) {
    throw new Error(
      "Set the variable SLACK_WEBHOOK_LINK in env to notify the missing link in the task"
    );
  }

  return {
    sendMissingTask(trelloCard) {
      const text = `
            Questa card Trello non ha il link Odoo:
            Nome: ${trelloCard.name}
            Url: ${trelloCard.shortUrl}`
      return axios.post(
        slackLink,
        {
          text
        },
        {
          headers: {
            "Content-type": "application/json"
          }
        }
      );
    }
  };
};
