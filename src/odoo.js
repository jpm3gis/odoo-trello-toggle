const { delay } = require("./lib");

function printTask(task) {
  return `named - ${task.info.name}\nlink - ${task.link}\ndata - ${task.info.date}\nhours - ${task.info.tot}`;
}

async function login(page, yourEmail, yourPassword) {
  await page.goto("https://odoo.r3-gis.com/web/login");
  return page
    .evaluate(
      (email, password) => {
        document.querySelector("#login").value = email;
        document.querySelector("#password").value = password;
        document.querySelector('[type="submit"]').click();
      },
      yourEmail,
      yourPassword
    )
    .then(delay(2000));
}

async function insertIntask(page, link, { date, tot, name }) {
  await page.goto(link).then(delay(5000));
  const evaluationResult = await page.evaluate(
    (dateTask, totHours, note) => {
      const DateTask = new Date(dateTask);

      // Format the date in order to be compatible with odoo
      function appendzero(str) {
        return `${str > 9 ? str : `0${str}`}`;
      }
      function formatdate(date) {
        return `${appendzero(date.getDate())}/${appendzero(
          date.getMonth() + 1
        )}/${date.getFullYear()} ${appendzero(date.getHours())}:${appendzero(
          date.getMinutes()
        )}:${appendzero(date.getSeconds())}`;
      }

      // Check if the task has already been added for this specific date
      function hasBeenAddedThisTaskInDate(textNote, tskDate) {
        const tbody = document.querySelector("table.oe_list_content > tbody");
        let trs = [];
        if (tbody) {
          trs = tbody.children;
        }
        for (const tr of trs) {
          const nameEntry = tr.querySelector('[data-field="name"]');
          if (nameEntry && textNote === nameEntry.textContent) {
            const dataFiled = tr.querySelector('[data-field="date"]');
            const dateEntry = new Date(
              dataFiled ? dataFiled.textContent : null
            );
            if (
              tskDate.getDate() === dateEntry.getDate() &&
              tskDate.getMonth() === dateEntry.getMonth() &&
              tskDate.getFullYear() === dateEntry.getFullYear()
            ) {
              return true;
            }
          }
        }
        return false;
      }

      if (!hasBeenAddedThisTaskInDate(note, DateTask)) {
        // Modify
        document.querySelector('[accesskey="E"]').click();
        setTimeout(function() {
          // Add a new line
          document
            .querySelector(".oe_form_field_one2many_list_row_add > a")
            .click();
          setTimeout(function() {
            // Insert data
            document.querySelector(
              '[data-fieldname="name"]'
            ).children[0].value = note;
            document.querySelector(
              '[data-fieldname="hours"]'
            ).children[0].value = totHours;
            document.querySelector(
              '.oe_datepicker_master[name="date"]'
            ).value = formatdate(DateTask);
            setTimeout(function() {
              // Save
              document.querySelector('[accesskey="S"]').click();
            }, 500);
          }, 500);
        }, 500);
      } else {
        return "NO_ADDITION";
      }
    },
    date.getTime(),
    tot,
    name
  );
  if (evaluationResult === "NO_ADDITION") {
    console.log(`Task named -- ${name} -- has already been entered in odoo`);
  }
  await delay(2000)();
}

async function isTaskAlreadyBeenInserted(page, link, { date, tot, name }) {
  await page.goto(link).then(delay(5000));
  const evaluationResult = await page.evaluate(
    (dateTask, totHours, note) => {
      const DateTask = new Date(dateTask);

      // Check if the task has already been added for this specific date
      function hasBeenAddedThisTaskInDate(textNote, tskDate) {
        const tbody = document.querySelector("table.oe_list_content > tbody");
        let trs = [];
        if (tbody) {
          trs = tbody.children;
        }
        for (const tr of trs) {
          const nameEntry = tr.querySelector('[data-field="name"]');
          if (nameEntry && textNote === nameEntry.textContent) {
            const dataFiled = tr.querySelector('[data-field="date"]');
            const dateEntry = new Date(
              dataFiled ? dataFiled.textContent : null
            );
            if (
              tskDate.getDate() === dateEntry.getDate() &&
              tskDate.getMonth() === dateEntry.getMonth() &&
              tskDate.getFullYear() === dateEntry.getFullYear()
            ) {
              return true;
            }
          }
        }
        return false;
      }

      if (hasBeenAddedThisTaskInDate(note, DateTask)) {
        return "INSERTED";
      } else {
        return "NOT_PRESENT";
      }
    },
    date.getTime(),
    tot,
    name
  );
  await delay(2000)();
  return evaluationResult === "INSERTED";
}

module.exports = {
  login,
  insertIntask,
  printTask,
  isTaskAlreadyBeenInserted
};
