class MissingAttachmentError extends Error {
    constructor(trelloCard) {
        const msg = `
            Trello card info:\n
                Name: ${trelloCard.name}\n
                Url: ${trelloCard.shortUrl}\n`
        super(msg)
    }
}

module.exports = {
    MissingAttachmentError
}