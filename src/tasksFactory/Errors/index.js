const { MissingAttachmentError } = require('./MissingAttachmentError')

module.exports = {
    MissingAttachmentError
}