const { readFile } = require('fs')
const { resolve } = require('path')

module.exports = (basepath) => ({
  extract() {
    const filepath = resolve(basepath, 'tasks.json')
    console.log('TASK_FILE_READER: ', `Reading from ${filepath}`)
    return new Promise((res, rej) => {
      readFile(filepath, (err, content) => {
        if(err) {
          return rej(err)
        }
        res(JSON.parse(content.toString()))
      })
    })
  }
})