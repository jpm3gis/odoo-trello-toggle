const makeFileTaskReader = require('./file')
const makeToggleTaskReader = require('./toggle')
const makeReunionTask = require('./reunion')
const makeBoardTask = require('./trello')

const TASKS_DATASOURCES = {
  TOGGLE: 'Toggle',
  REUNION: 'Reunion',
  BOARD: 'Board',
  FILE: 'File'
}

function tasksFactory(config) {
  console.log('TASK_FACTORY: ', `Using ${config.tasks_datasource || 'File'} as a datasource`)
  switch (config.tasks_datasource) {
    case TASKS_DATASOURCES.TOGGLE:
      return makeToggleTaskReader({
        toggle_api_key: config.task_toggle_api_key,
        trello_api_key: config.task_trello_api_key,
        trello_api_token: config.task_trello_oauth_token,
        tasks_start_date: config.tasks_start_date,
        tasks_end_date: config.tasks_end_date
      })
    case TASKS_DATASOURCES.REUNION:
      return makeReunionTask({
        days: config.number_of_days,
        time: config.time_for_reunion
      })
    case TASKS_DATASOURCES.BOARD:
      return makeBoardTask({
        task_trello_api_key: config.task_trello_api_key,
        task_trello_oauth_token: config.task_trello_oauth_token,
        boardId: config.boardId
      })
    default:
      return makeFileTaskReader(config.tasks_base_path_dir || process.cwd())
  }
}

module.exports = {
  tasksFactory,
  TASKS_DATASOURCES
}