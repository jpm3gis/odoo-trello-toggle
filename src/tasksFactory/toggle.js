const axios = require("axios").default;
const { appendzero } = require("../lib");
const { MissingAttachmentError } = require("./Errors");
const makeNotifier = require("../notifier");

/*
  Task
  {
    link: String -> link della pagina della task in odoo,
    info: {
      date: Data in qualunque formato compatibile con js,
      tot: Il totale delle ore in formato HH:MM,
      name: Il nome della task svolta
    } 
  }
*/

function intoTaksFormat(togglTimeEntry) {
  return {
    link: null,
    info: {
      date: new Date(togglTimeEntry.start),
      end: new Date(togglTimeEntry.stop),
      name: togglTimeEntry.description,
      tot: togglTimeEntry.duration,
    },
  };
}

function intoTaskWithDurationFormatted(task) {
  return {
    ...task,
    info: {
      ...task.info,
      tot: formatDuration(task.info.tot),
    },
  };
}

function formatDuration(durationInSecond) {
  const minutes = Math.floor(durationInSecond / 60);
  const hour = Math.floor(minutes / 60);
  const diffMinutes = minutes - hour * 60;
  return `${appendzero(hour)}:${appendzero(diffMinutes)}`;
}

function isNotRunning(togglTimeEntry) {
  return !!togglTimeEntry.stop;
}

function cluster(list) {
  let workon = [...list];
  let newList = [];
  workon.forEach((timeEntry) => {
    let foundEl = newList.find(isTheSameTaskInsideDayPeriod(timeEntry));
    if (foundEl) {
      foundEl.info.tot = foundEl.info.tot + timeEntry.info.tot;
    } else {
      newList.push(timeEntry);
    }
  });
  return newList;
}

function isTheSameTaskInsideDayPeriod(oneTask) {
  const twelveHourInMilliseconds = 43200000;
  return (secondTask) =>
    secondTask.info.name === oneTask.info.name &&
    Math.abs(secondTask.info.end.getTime() - oneTask.info.date.getTime()) <
      twelveHourInMilliseconds;
}

function withTrelloLink({ key, token }, notifier) {
  return (task) =>
    axios
      .get("https://api.trello.com/1/search", {
        params: {
          key,
          token,
          query: task.info.name,
          modelTypes: "cards",
        },
      })
      .then((resp) => resp.data)
      .then((data) => data.cards)
      .then((cards) => {
        if (Array.isArray(cards) && cards.length > 0) {
          return cards[0];
        } else {
          throw new Error(`Card named -- ${task.info.name} -- Not found`);
        }
      })
      .then((card) =>
        axios
          .get(`https://api.trello.com/1/cards/${card.id}/attachments`, {
            params: {
              key,
              token,
            },
          })
          .then((resp) => resp.data)
          .then((attachments) =>
            Array.isArray(attachments) ? attachments : []
          )
          .then((attachments) =>
            attachments.filter((attachment) =>
              /https:\/\/odoo\.r3-gis\.com[\S\s]*/gm.test(attachment.url)
            )
          )
          .then((attachments) => attachments.pop())
          .then((attachment) => {
            if (attachment) {
              return attachment;
            } else {
              console.log(
                `Card named -- ${card.name} -- does't have a link to odoo, sending slack message to Adriana...`
              );
              return notifier
                .sendMissingTask(card)
                .then(() => {
                  throw new MissingAttachmentError(card);
                })
                .catch(() => {
                  throw new MissingAttachmentError(card);
                });
            }
          })
          .then((attachment) => ({
            ...task,
            link: attachment.url,
          }))
      )
      .catch((err) => console.log(err.message));
}

module.exports = ({
  toggle_api_key,
  trello_api_key,
  trello_api_token,
  tasks_start_date,
  tasks_end_date,
}) => ({
  /*
  {
    id: 1382633630,
    guid: '47ac64a5446fbe9be368b71f119e20a2',
    wid: 3761549,
    pid: 155017427,
    billable: false,
    start: '2019-12-05T15:25:07+00:00',
    stop: '2019-12-05T16:41:49+00:00',
    duration: 4602,
    description: '(2) [life] C.4 Analisi e test cruscotto meteo',
    duronly: false,
    at: '2019-12-05T16:41:49+00:00',
    uid: 5191898
  }
  */
  extract() {
    const notifier = makeNotifier();
    return axios
      .get(
        `https://www.toggl.com/api/v8/time_entries?start_date=${new Date(
          tasks_start_date
        ).toISOString()}&end_date=${new Date(tasks_end_date).toISOString()}`,
        {
          auth: {
            username: toggle_api_key,
            password: "api_token",
          },
        }
      )
      .then((res) => (Array.isArray(res.data) ? res.data : []))
      .then((list) => list.filter(isNotRunning))
      .then((list) => list.map(intoTaksFormat))
      .then((list) => cluster(list))
      .then((list) => list.map(intoTaskWithDurationFormatted))
      .then((list) =>
        list.map(
          withTrelloLink(
            {
              token: trello_api_token,
              key: trello_api_key,
            },
            notifier
          )
        )
      )
      .then(async (proms) => {
        const result = [];
        for await (let res of proms) {
          result.push(res);
        }
        return result;
      })
      .then((list) => list.filter((element) => !!element))
      .catch((err) => console.log("TASK_TOGGLE_READER: ", err));
  },
});
