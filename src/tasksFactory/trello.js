const axios = require("axios").default;

module.exports = function({
  task_trello_api_key,
  task_trello_oauth_token,
  boardId
}) {
  return {
    async extract() {
      // Use the sprint board
      const resp = await axios.get(
        `https://api.trello.com/1/boards/${boardId}/cards`,
        {
          params: {
            key: task_trello_api_key,
            token: task_trello_oauth_token,
            fields: "name,shortUrl"
          }
        }
      );
      const cards = resp.data;
      const attachments = [];
      for (let index = 0; index < cards.length; index++) {
        const card = cards[index];
        const attachment = await axios
          .get(`https://api.trello.com/1/cards/${card.id}/attachments`, {
            params: {
              key: task_trello_api_key,
              token: task_trello_oauth_token
            }
          })
          .then(a => a.data)
          .catch(e => null)
        attachments.push(attachment ? attachment : [])
      }
      const cardAndAttachments = cards.map((card, index) => ({
        ...card,
        attachments: attachments[index].filter(attachment =>
          /https:\/\/odoo\.r3-gis\.com[\S\s]*/gm.test(attachment.url)
        )
      }));
      return cardAndAttachments.map(card => ({
          link: card.attachments[card.attachments.length - 1] ? card.attachments[card.attachments.length - 1].url : null,
          info: {
            date: null,
            end: null,
            name: card.name,
            tot: null
          }
      }))
    }
  };
};
