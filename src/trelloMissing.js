const axios = require("axios").default;

module.exports = function(
  { task_trello_api_key, task_trello_oauth_token },
  notifier
) {
  return async (boardId, onlyPrint = false) => {
    // Use the sprint board
    const resp = await axios.get(
      `https://api.trello.com/1/boards/${boardId}/cards`,
      {
        params: {
          key: task_trello_api_key,
          token: task_trello_oauth_token,
          fields: "name,shortUrl"
        }
      }
    );
    const cards = resp.data;
    let attachments = [];
    for (let index = 0; index < cards.length; index++) {
      const card = cards[index];
      const result = await axios
        .get(`https://api.trello.com/1/cards/${card.id}/attachments`, {
          params: {
            key: task_trello_api_key,
            token: task_trello_oauth_token
          }
        })
        .then(a => a.data);
      attachments.push(result);
    }

    const cardAndAttachments = cards.map((card, index) => ({
      ...card,
      attachments: attachments[index]
    }));
    // list the task
    const missingOdooLink = cardAndAttachments.filter(
      ({ attachments }) =>
        attachments.filter(attachment =>
          /https:\/\/odoo\.r3-gis\.com[\S\s]*/gm.test(attachment.url)
        ).length === 0
    );
    // send those task using the notifier
    for (let index = 0; index < missingOdooLink.length; index++) {
      const trelloCard = missingOdooLink[index];
      if (onlyPrint) {
        console.log(trelloCard);
      } else {
        await notifier.sendMissingTask(trelloCard);
      }
    }
    return missingOdooLink;
  };
};
