require('dotenv').config()
const makeNotifier = require('./src/notifier')

const notifier = makeNotifier();

(async () => {
    const trelloCard = {
        shortUrl: 'https://trello.com/c/BL68Rogl/765-1-lifemeteofe-implementazione-servizio-lettura-configurazioni-e-api',
        name: 'Test card'
    }
    await notifier.sendMissingTask(trelloCard)
})()